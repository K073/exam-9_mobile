import React from 'react';
import ContactList from "./containers/ContactList/ContactList";
import {applyMiddleware, createStore} from "redux";
import thunkMiddleware from 'redux-thunk';
import reducer from "./store/reducer";
import {Provider} from "react-redux";
import {View} from "react-native";

const store = createStore(reducer, applyMiddleware(thunkMiddleware));

export default class App extends React.Component{
  render(){
    return (
      <View style={{marginTop: 22}}>
        <Provider store={store}>
          <ContactList/>
        </Provider>
      </View>
    )
  }
}