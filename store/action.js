import axios from '../axios'

export const FETCH_CONTACT_REQUEST = 'FETCH_CONTACT_REQUEST';
export const FETCH_CONTACT_SUCCESS = 'FETCH_CONTACT_SUCCESS';
export const FETCH_CONTACT_ERROR = 'FETCH_CONTACT_ERROR';

export const fetchContactRequest = () => {
  return { type: FETCH_CONTACT_REQUEST };
};

export const fetchContactSuccess = (data) => {
  return { type: FETCH_CONTACT_SUCCESS, data};
};

export const fetchContactError = () => {
  return { type: FETCH_CONTACT_ERROR };
};

export const fetchContact = () => {
  return dispatch => {
    dispatch(fetchContactRequest());
    axios.get('/.json').then(response => {
      dispatch(fetchContactSuccess(response.data));
    }, error => {
      dispatch(fetchContactError());
    });
  }
};