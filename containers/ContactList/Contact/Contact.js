import React from 'react';
import {Image, Modal, Text, TouchableHighlight, View} from "react-native";

class Contact extends React.Component {
  state = {
    modalVisible: false
  };

  setModalVisible (visible) {
    this.setState({modalVisible: visible});
  }

  render() {
     return (
       <View>
         <TouchableHighlight
           onPress={() => {
             this.setModalVisible(true);
           }}>
           <View style={{width: '100%', flexDirection: 'row', padding: 10}}>
             <Image
               source={{uri: this.props.img ? this.props.img : 'https://i1.wp.com/www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png?resize=256%2C256&quality=100'}}
               style={{width: 40, height:40, padding: 5}}
             />
             <Text style={{padding: 5}}>
               {this.props.name}
             </Text>
           </View>
         </TouchableHighlight>
         <Modal
           animationType="slide"
           transparent={false}
           visible={this.state.modalVisible}
           onRequestClose={() => {
             this.setModalVisible(!this.state.modalVisible);
           }}
         >
           <View>
             <View>
               <Image
                 source={{uri: this.props.img ? this.props.img : 'https://i1.wp.com/www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png?resize=256%2C256&quality=100'}}
                 style={{width: 390, height: 300, padding: 5}}
               />
               <Text>{this.props.name}</Text>
               <Text>{this.props.phone}</Text>
               <Text>{this.props.email}</Text>
               <TouchableHighlight
                 onPress={() => {
                   this.setModalVisible(!this.state.modalVisible);
                 }}>
                 <Text style={{width: '100%', textAlign: 'center', borderWidth: 1, padding: 5}}>Go back</Text>
               </TouchableHighlight>
             </View>
           </View>
         </Modal>
       </View>
     );
  }
}

export default Contact