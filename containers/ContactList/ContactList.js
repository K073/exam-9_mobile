import React from 'react';
import {ScrollView} from "react-native";
import {connect} from "react-redux";
import {fetchContact} from "../../store/action";
import Contact from "./Contact/Contact";

class ContactList extends React.Component {

  componentDidMount() {
    this.props.fetchContact();
  }

  render() {
    return (
      <ScrollView>
        {Object.keys(this.props.contacts).map(key => {
          return <Contact
            key={key}
            id={key}
            img={this.props.contacts[key].photo}
            name={this.props.contacts[key].name}
            phone={this.props.contacts[key].phone}
            email={this.props.contacts[key].email}
          />
        })}
      </ScrollView>
    );
  }
}

const mapStateToProps = state => {
  return {
    contacts: state.contacts
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchContact: () => dispatch(fetchContact())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ContactList);